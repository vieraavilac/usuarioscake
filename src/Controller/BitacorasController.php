<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bitacora Controller
 *
 * @property \App\Model\Table\BitacoraTable $Bitacora
 *
 * @method \App\Model\Entity\Bitacora[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BitacorasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
   
    public function index()
    {
        $bitacora = $this->paginate($this->Bitacora);

        $this->set(compact('bitacora'));
    }

    /**
     * View method
     *
     * @param string|null $id Bitacora id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 
    public function view($id = null)
    {
        $bitacora = $this->Bitacora->get($id, [
            'contain' => []
        ]);

        $this->set('bitacora', $bitacora);
    }


     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bitacora = $this->Bitacoras->newEntity();
        if ($this->request->is('post')) {
            // $bitacora = $this->Bitacoras->patchEntity($bitacora, $this->request->getData());
            if ($this->Bitacoras->save($bitacora)) {
                $this->Flash->success(__('Wardado en la bitacora.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bitacora could not be saved. Please, try again.'));
        }
        $this->set(compact('bitacora'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bitacora id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
  
    public function edit($id = null)
    {
        $bitacora = $this->Bitacora->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bitacora = $this->Bitacora->patchEntity($bitacora, $this->request->getData());
            if ($this->Bitacora->save($bitacora)) {
                $this->Flash->success(__('The bitacora has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bitacora could not be saved. Please, try again.'));
        }
        $this->set(compact('bitacora'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bitacora id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bitacora = $this->Bitacora->get($id);
        if ($this->Bitacora->delete($bitacora)) {
            $this->Flash->success(__('The bitacora has been deleted.'));
     }   } else {
            $this->Flash->error(__('The bitacora could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    */
}
