<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */


    // public function beforeRender(Event $event){
    //     $this->viewBuilder()->theme('Gentelella');
    // }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Flash');
        $this->Auth->allow(['logout', 'index', 'contrasenia', 'restablecer']);//, 'view']);
        $habilitado = $this->getRequest()->getSession()->read('Usuario.habilitado'); 
        $rol = $this->getRequest()->getSession()->read('Usuario.administrador');
        if($habilitado == true && $rol == false){
            $this->Auth->allow(['index', 'logout']);
        }
    }

//qvbyFYP6xh
    public function initialize(){
   
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,     // true/false
            'sitekey' => '6LeL6ooUAAAAAFo6Ht1GrGoes5JAeFAFKJ2ks8Fn', //if you don't have, get one: https://www.google.com/recaptcha/intro/index.html
            'secret' => '6LeL6ooUAAAAALtW3IlusIB0HWRESlVLW8x8CC8i',
            'type' => 'image',  // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'en',      // default en
            'size' => 'normal'  // normal/compact
        ]);

        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'userModel' => 'Usuarios'
                ],
            ],
            'loginAction' => [
                'controller' => 'Usuarios',
                'action' => 'login'

            ],

            'loginRedirect' => [
                'controller' => 'Usuarios', 
                'action' => 'index'

            ],
             //use isAuthorized in Controllers
            //'authorize' => 'Controller',
             // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => [
                'controller' => 'Usuarios', 
                'action' => 'index'

            ]

            //$this->referer()
        ]);

        //$this->Auth->deny();
    }

    // public function isAuthorized($usuario = null){
    //     if(($usuario['habilitado']) == true && $usuario['administrador'] == true){
    //         return true;
    //     } 

    // }


}
