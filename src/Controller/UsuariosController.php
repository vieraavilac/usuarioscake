<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
//use Cake\Mailer\Email; 
use Cake\Mailer\MailerAwareTrait;        
use Cake\Network\Email\Email; 
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use DateTime;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Auth;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Usuarios Controller
 *
 * @property \App\Model\Table\UsuariosTable $Usuarios
 *
 * @method \App\Model\Entity\Usuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsuariosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        if ($this->request->getParam('action') === 'contact') {
                $this->loadComponent('Recaptcha.Recaptcha');
            }

        // $habilitado = $this->getRequest()->getSession()->read('Usuario.habilitado'); 
   //      $rol = $this->getRequest()->getSession()->read('Usuario.administrador');
             $this->Auth->allow(['logout', 'login']);

       
    }

    public $paginate = [

        'limit' => 10,
        'order' => [
            'Usuarios.apellidopaterno' => 'ASC',
            'Usuarios.apellidomaterno' => 'ASC',
            'Usuarios.nombre' => 'ASC',
            'Usuarios.email' => 'ASC'
            ]
    ];



    public function index(){
        $this->loadComponent('Paginator');
        $usuarios = $this->/*Paginator->*/paginate($this->Usuarios->find('all'));
       
        $this->set(compact('usuarios'));

    }


   
    /**
     * View method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */





    public function view($id = null){
        $usuario = $this->Usuarios->findByIdusuario($id)->firstOrFail();
//$usuarios = $this->Usuarios->findByIdusuario($idUsuario)->firstOrFail();
        $this->set(compact('usuario'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    //use MailerAwareTrait;
    public function correo(){

        $this->autoRender = false;

        Email::configTransport('mail', [
        'host' => 'ssl://smtp.gmail.com',
        'port' => 465,
        'username' => 'crud.usuarios.cva@gmail.com',
        'password' => '.Hola123.',
        'className' => 'SMTP',
        ]);
    
    }



    public function add(){
        // $this->viewBuilder()->setLayout('add');
        $usuario = $this->Usuarios->newEntity();
        //$this->Usuarios->contrasenia();
        //$mailer = new UsuarioMailer();    


        if ($this->request->is('post')) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());

            // debug($usuario);
            // exit;
            //$fkUsuario = $this->request->getData('Usuarios.idusuario');
            if ($this->Usuarios->save($usuario)) {

                // 22/01/2019
                $bitacorasTable = TableRegistry::get('Bitacoras');
                $fkUsuario = $this->Auth->user('idusuario');;// $this->request->getData('idusuario');
                $bitacora = $bitacorasTable->newEntity();//$this->request->getData(), [ 'associated' => 'Usu']);
                $bitacora->accion = 'Registro de usuario';
                $bitacora->idusuario = $fkUsuario;
                $bitacorasTable->save($bitacora);

        

                $correo = new Email();
                $correo-> to($this->request->getData('email'))
                    //->transport('mail')
                    ->template('registro')
                    ->emailFormat('html')
                    ->from('crud.usuarios.cva@gmail.com')
                    ->subject('Bienvenido')
                    ->replyTo('crud.usuarios.cva@gmail.com', 'CRUD')
                    ->setViewVars([
                        'nombre' => $this->request->getData('nombre'), 'apellidopaterno' =>$this->request->getData('apellidopaterno'), 'apellidomaterno' => $this->request->getData('apellidomaterno'),
                         'contrasenia' => $this->request->getData('password')])
                    ->send('registro', $usuario)         
                         ;
; 
                $this->Flash->success(__('Usuario guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar el usuario. Intente de nuevo.'));
        }
        $this->set(compact('usuario'));
    }

    /** 
     * Edit method *cI4f%XSbX
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($idUsuario){
        $usuario = $this->Usuarios->get($idUsuario, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());
            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('Usuario guardado.'));

                $bitacorasTable = TableRegistry::get('Bitacoras');
                $fkUsuario = $this->Auth->user('idusuario');// $this->request->getData('idusuario');
                $bitacora = $bitacorasTable->newEntity();//$this->request->getData(), [ 'associated' => 'Usu']);
                $bitacora->accion = 'Modificacion de usuario';
                $bitacora->idusuario = $fkUsuario;
                $bitacorasTable->save($bitacora);


                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar el usuario. Intente de nuevo.'));
        }
        $this->set(compact('usuario'));
    }


    /**
     * Delete method 
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($idUsuario){

        $this->request->allowMethod(['post', 'delete']);
        $usuario = $this->Usuarios->findByIdusuario($idUsuario)->firstOrFail();

        $usuario = $this->Usuarios->get($idUsuario);
        if ($this->Usuarios->delete($usuario)) {

                $bitacorasTable = TableRegistry::get('Bitacoras');
                $fkUsuario = $this->Auth->user('idusuario');
                $bitacora = $bitacorasTable->newEntity();//$this->request->getData(), [ 'associated' => 'Usu']);
                $bitacora->accion = 'Eliminacion de usuario';
                $bitacora->idusuario = $fkUsuario;
                $bitacorasTable->save($bitacora);


            $this->Flash->success(__('Usuario eliminado.'));
        } else {
            $this->Flash->error(__('No se pudo eliminar el usuario. Intente de nuevo.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login(){
        if ($this->request->is('post')) {
            if ($this->Recaptcha->verify()) {
                $usuario = $this->Auth->identify();
                if ($usuario['habilitado']) {
                    $this->Auth->setUser($usuario);

                    $bitacorasTable = TableRegistry::get('Bitacoras');
                    $fkUsuario = $this->Auth->user('idusuario');// $this->request->getData('idusuario');
                    $bitacora = $bitacorasTable->newEntity();//$this->request->getData(), [ 'associated' => 'Usu']);
                    $bitacora->accion = 'Login';
                    $bitacora->idusuario = $fkUsuario;
                    $bitacorasTable->save($bitacora);


                    return $this->redirect($this->Auth->redirectUrl());//(['controller' => 'Usuarios', 'action' => 'index']);
                } else if ($usuario['habilitado'] === false) {
                     $this->Flash->error('Usuario deshabilitado.');
                } else
                $this->Flash->error('Usuario o contraseña incorrecto.');
            }
            $this->Flash->default(__('Verifica el reCAPTCHA'));
        }

    }


    public function logout(){
        
        $this->Flash->success('Sesión cerrada.');

        $bitacorasTable = TableRegistry::get('Bitacoras');
        $fkUsuario = $this->Auth->user('idusuario');
        $bitacora = $bitacorasTable->newEntity();
        $bitacora->accion = 'Logout';
        $bitacora->idusuario = $fkUsuario;
        $bitacorasTable->save($bitacora);

        return $this->redirect($this->Auth->logout());

       
    }

    public function isAuthorized($usuario = null){
        if($usuario['administrador'] === false){
            $allowedActions = ['index', 'logout', 'cambio'];//, 'view'];
            if(in_array($this->request->action, $allowedActions)) {
                return true;
            }
        }

        if($usuario['administrador']){
                return true;           
        }
        return false;

    }
    
    public function contrasenia(){
        if($this->request->is('post')){

            $usuarios = TableRegistry::get('Usuarios');
            //$usuario = $usuariosTable->findByEmail($this->request->getData('email'));


            $query = $this->Usuarios->findByEmail($this->request->getData('email'));

            $usuario = $query->first();
            // $usuario->timeout = date('Y-m-d H:i:s');
            // debug($usuario);
            // exit();
            if (is_null($usuario)){
                $this->Flash->error('El correo introducido no existe. Intenta de nuevo.');
            } else {

                $contrasenia = $usuario->password;

                //taimaut
                $tiempo = time() + DAY;
                $timeout = date('Y-m-d H:i:s', $tiempo);
                $usuario->timeout = $timeout;
                //$usuario->password = $contrasenia;
                

                if (/*$usuarios->save($usuario)*/$this->Usuarios->updateAll(['password' => $contrasenia, 'timeout' => $timeout], ['idusuario' => $usuario->idusuario])){
                        
                        $url = Router::url(['controller' => 'Usuarios', 'action' => 'restablecer', '_full' => true, $contrasenia]);
                        $this->mandarEmailcontrasenia($contrasenia, $usuario, $url);
                       
                        $bitacorasTable = TableRegistry::get('Bitacoras');
                        $fkUsuario = $usuario->idusuario;// $this->request->getData('idusuario');
                        $bitacora = $bitacorasTable->newEntity();//$this->request->getData(), [ 'associated' => 'Usu']);
                        $bitacora->accion = 'Solicitud de recuperacion de contrasenia';
                        $bitacora->idusuario = $fkUsuario;
                        $bitacorasTable->save($bitacora);
                        $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error('Ocurrió un error al restablecer la contraseña / Timeout');
                }
            }
        }
    }

    private function mandarEmailcontrasenia($contrasenia, $usuario, $url){
  
        $correo = new Email();
        $correo->template('restablecer')
               ->emailFormat('html')
               ->from('crud.usuarios.cva@gmail.com')
               ->to($usuario->email)
               ->subject('Restablecer contraseña')
               ->setViewVars(['apellidopaterno' => $usuario->apellidopaterno, 'apellidomaterno' => $usuario->apellidomaterno, 'nombre' => $usuario->nombre, 'contrasenia' => $contrasenia, 'url' => $url]);
               if($correo->send()){
                    $this->Flash->success(__('Verifica tu bandeja de entrada para obtener tu nueva contraseña.'));

               } else {
                    $this->Flash->success(__('Ocurrió un error al enviar tu correo: ') . $correo->smtpError);
               }
    }

    public function restablecer($contrasenia = null){
        if($contrasenia){
            $usuarios = TableRegistry::get('Usuarios');
            
            $query = $this->Usuarios->find('all', ['conditions' => ['password LIKE' => $contrasenia . '%', 'timeout >' => date('Y-m-d H:i:s', time())]]);

            
            $usuario = $query->first();
            

            if($usuario){
                if(!empty($this->request->getData())){
               
                    $this->request->data['timeout'] = null;
                    
                    $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());
                
                    $usuarios->patchEntity($usuario, $this->request->getData());
                    if($this->Usuarios->save($usuario)){
                        $this->Flash->success('Contraseña actualizada.');
                        //bitacorina, ah qué buena medicina 
                        $bitacorasTable = TableRegistry::get('Bitacoras');

                        $fkUsuario = $usuario->idusuario;
                        $bitacora = $bitacorasTable->newEntity();
                        $bitacora->accion = 'Cambio de contrasenia';
                        $bitacora->idusuario = $fkUsuario;
                        $bitacorasTable->save($bitacora);

                        return $this->redirect(['action' => 'login']);

                    } else {
                        $this->Flash->error('No se pudo restablecer la contraseña.');
                    }
                }
 
            } else {
                $this->Flash->error('Contraseña inválida o expirada. Verifica tu correo de nuevo o intenta nuevamente.');
                $this->redirect(['action' => 'contrasenia']);         
            }
            unset($usuario->password);
            $this->set(compact('usuario'));
        } else {
            $this->redirect(['action' => 'login']);
        }

    }

     public function cambio(){

        $usuarios = TableRegistry::get('Usuarios');
        $idUsuario = $this->Auth->user('idusuario');
        $usuario = $this->Usuarios->get($idUsuario, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if((new DefaultPasswordHasher)->check($this->request->data['passwordactual'], $usuario->password)){
                    $nuevaC = $this->request->data['password2'];
                    
                    $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData(), ['validate' => 'passwords', 'validate' => 'robustez']);

                    $usuario->password = $nuevaC;
                   

                if ($this->Usuarios->save($usuario)) {
                        

                        $this->Flash->success(__('Contraseña guardada.'));

                        $bitacorasTable = TableRegistry::get('Bitacoras');
                        $fkUsuario = $this->Auth->user('idusuario');// $this->request->getData('idusuario');
                        $bitacora = $bitacorasTable->newEntity();//$this->request->getData(), [ 'associated' => 'Usu']);
                        $bitacora->accion = 'Cambio de contrasenia';
                        $bitacora->idusuario = $fkUsuario;
                        $bitacorasTable->save($bitacora);


                        return $this->redirect(['action' => 'index']);
                }
                //$this->Flash->error(__('No se pudo guardar el usuario. Intente de nuevo.'));
            }
                
            $this->Flash->error(__('Contraseña actual incorrecta.'));

        }
        $this->set(compact('usuario'));

    }




}
