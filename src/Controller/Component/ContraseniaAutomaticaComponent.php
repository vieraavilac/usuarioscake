<? php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ContraseniaAutomaticaComponent extends Component{
	
	public function generarContrasenia{

        $base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz';
        $base .= '1234567890';
        $base .= '!@#%.,*?-_';

        $contrasenia = '';
        $limite = strlen($base) - 1;

        for ($i=0; $i < 15; $i++)
            $contrasenia .= $base[rand(0,$limite)];

        return $contrasenia;

	}
}