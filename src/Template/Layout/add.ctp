<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Añadir usuario</title>
		<link rel="stylesheet" href="../../webroot/css/main.css" />
		<noscript><link rel="stylesheet" href="../../webroot/css/noscript.css" /></noscript>
		<?php $this->element('head') ?>
	</head>
	<body class="contact is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<?php $this->element('header') ?>
				</header>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-user-circle"></span>
						<h2>Nuevo Usuario</h2>
					</header>

					<!-- One -->
						<section class="wrapper style4 special container medium">

							<!-- Content -->
								<div class="content">

									<?php
							            echo $this->Form->create($usuario, ['type' => 'file']); ?> 
							            <div class="row gtr-50">
											<div class="col-6 col-12-mobile">
											<?php
							           		 echo $this->Form->input('photo', ['type' => 'file']);
							           		 ?> </div>
											<div class="col-6 col-12-mobile">
											<?php
							            	  echo $this->Form->control('email', ['type' => 'email']);
							            //echo $this->Html->link('Generar password', ['class' => 'button', 'action' => 'contrasenia']);
							           // echo $this->Form->button(__('Generar Contraseña'));
							            	 ?> </div>
											<div class="col-6 col-12-mobile"> 
											<?php
							            		echo $this->Form->control('accion', ['value' => 'Registro de usuario', 'type' => 'hidden']);
							            		echo $this->Form->control('password', ['value' => $contrasenia, 'type' => 'hidden']);
							               ?></div>
											<div class="col-6 col-12-mobile"> <?php
							            		echo $this->Form->control('apellidopaterno');
							            	?></div>
											<div class="col-6 col-12-mobile"> <?php
							            		echo $this->Form->control('apellidomaterno');
							            	?></div>
											<div class="col-6 col-12-mobile"> <?php
							            		echo $this->Form->control('nombre');
							            	?></div>
											<div class="col-6 col-12-mobile"> <?php
							            		echo $this->Form->control('habilitado');
							            	?></div>
											<div class="col-6 col-12-mobile"> <?php
							            		echo $this->Form->control('administrador');
							            	?></div>
											<div class="col-6 col-12-mobile"> <?php
							            		echo $this->Form->control('Tu password es', ['value' => $contrasenia, 'disabled']); ?>
							            	</div>

								</div>

						</section>

				</article>

			<!-- Footer -->
				<?php $this->element('footer') ?>

		</div>

		<!-- Scripts -->
			<?php $this->element('scripts') ?>

	</body>
</html>