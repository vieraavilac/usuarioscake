<title>Solicitar nueva contraseña</title>
<nav class="medium-2 medium-2 columns" id="actions-sidebar">
	<ul class="side-nav
		<li class="heading"><?= __('Acciones') ?></li>
		    <li><?= $this->Html->link(__('Iniciar sesión'), ['action' => 'login']) ?></li>
    </ul>
</nav>

<div class="usuarios form large-9 medium-8 columns content">
	<fieldset> <legend><?= __('Solicitar nueva contraseña') ?></legend> 
	<?php
    	echo $this->Form->create();
        echo $this->Form->input('email', ['autofocus' => true, 'label' => 'Email', 'required' => true]);
        ?>
    </fieldset>
	
    <?= $this->Form->button(__('Enviar link a mi correo')) ?>
    <?= $this->Form->end() ?>
</div>
