<script type="text/javascript">
    
    function mostrarFoto(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
</script>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
//function generarContrasenia(){

        $base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz';
        $base .= '1234567890';
        $base .= '!@#%.,*?-_';

        $contrasenia = '';
        $limite = strlen($base) - 1;

        for ($i=0; $i < 10; $i++)
            $contrasenia .= $base[rand(0,$limite)];

        //return $contrasenia;
//$this->layout = 'add';
?>
    <head>
        <title>Agregar usuario</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="css/main.css"/>
        <noscript><link rel="stylesheet" href="css/noscript.css" /></noscript>
    </head>
<nav class="medium-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Cerrar sesión'), ['action' => 'logout']) ?></li>
    </ul>
    
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    
    <fieldset>
        <legend><?= __('Agregar Usuario') ?></legend>
        <?php

            //echo $this->Form->input('photo', ['type' => 'file', 'onchange' => 'mostrarFoto($this)']);
            echo $this->Form->create($usuario, ['type' => 'file']);
            echo $this->Form->input('photo', ['type' => 'file']);
            $this->Html->scriptStart(['block' => true]);
                $this->Form->input('photo', ['type' => 'file', 'onchange' => 'mostrarFoto($this)']);
            
                $this->Html->image('http://oakclifffilmfestival.com/assets/placeholder-user-300x300.png');
            $this->Html->scriptEnd();
            echo "<script>mostrarFoto(this)</script>";
            echo $this->Form->control('email', ['type' => 'email']);
            //echo $this->Html->link('Generar password', ['class' => 'button', 'action' => 'contrasenia']);
           // echo $this->Form->button(__('Generar Contraseña'));
            echo $this->Form->control('accion', ['value' => 'Registro de usuario', 'type' => 'hidden']);
            echo $this->Form->control('password', ['value' => $contrasenia, 'type' => 'hidden']);
            echo $this->Form->control('apellidopaterno');
            echo $this->Form->control('apellidomaterno');
            echo $this->Form->control('nombre');
            echo $this->Form->control('habilitado');
            echo $this->Form->control('administrador');
            echo $this->Form->control('Tu password es', ['value' => $contrasenia, 'disabled']);
        ?>
    </fieldset>

    <div class="col-12">
        <ul class="buttons">
            <li><input type="submit" class="special" value="Enviar" /></li>
        </ul>
    </div>
    <?= $this->Form->end() ?>
    
</div>