<title>Cambiar contraseña</title>
<nav class="medium-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Cerrar sesión'), ['action' => 'logout']) ?></li>
    </ul>
</nav>

<div class="usuarios form large-10 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Nueva contraseña') ?></legend>
        <?php
        //ke
            echo $this->Form->input('passwordactual', ['label' => ['text' => 'Contraseña actual'], 'type' => 'password']);
            
            echo $this->Form->control('password1', ['label' => ['text' => 'Nueva contraseña'], 'type' => 'password']);
            
            echo $this->Form->control('password2', ['label' => ['text' => 'Confirmar nueva contraseña'], 'type' => 'password']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>