<title>Iniciar sesión</title>
<h1>Login</h1>

<?= $this->Form->create() ?>
<?= $this->Form->control('email') ?>
<?= $this->Form->control('password') ?>
<?= $this->Recaptcha->display() ?>  
<?= $this->Form->button('Login') ?>
<?= $this->Html->link('Olvidé mi contraseña', '/usuarios/contrasenia', ['class' => 'button']); ?>
<?= $this->Form->end() ?>
