<?php
        $base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz';
        $base .= '1234567890';
        $base .= '!@#%.,*?-_';

        $password = '';
        $limite = strlen($base) - 1;

        for ($i=0; $i < 10; $i++)
            $password .= $base[rand(0,$limite)];

        //return $password;
?>

<nav class="medium-2 medium-2 columns" id="actions-sidebar">
	<ul class="side-nav
		<li class="heading"><?= __('Acciones') ?></li>
		    <li><?= $this->Html->link(__('Iniciar sesión'), ['action' => 'login']) ?></li>
    </ul>
</nav>

<div class="usuarios form large-9 medium-8 columns content">
	<fieldset> <legend><?= __('Restablecer contraseña') ?></legend> 
	<?php
    	echo $this->Form->create();
        echo $this->Form->control('password', ['value' => $password, 'type' => 'hidden']);
        echo $this->Form->control('Tu nueva password es', ['value' => $password, 'disabled']);
        ?>
    </fieldset>
    
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
    
	
</div>
