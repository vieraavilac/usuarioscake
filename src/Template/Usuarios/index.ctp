<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario[]|\Cake\Collection\CollectionInterface $usuarios
 */
?>
<title>Listado de usuarios</title>
<nav class="medium-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo Usuario'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Cambiar contraseña'), ['action' => 'cambio']) ?></li>
        <li><?= $this->Html->link(__('Cerrar sesión'), ['action' => 'logout']) ?></li>
    </ul>
</nav>
<div class="usuarios index large-10 medium-8 columns content">
    <h3><?= __('Usuarios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('apellidopaterno', 'Apellido Paterno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apellidomaterno', 'Apellido Materno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre', 'Nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email', 'E-mail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Rol') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Estatus') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?= h($usuario->apellidopaterno) ?></td>
                <td><?= h($usuario->apellidomaterno) ?></td>
                <td><?= h($usuario->nombre) ?></td>
                <td><?= h($usuario->email) ?></td>
                <td><?= $usuario->administrador ? __('Administrador') : __('Usuario'); ?></td>
                <td><?= $usuario->habilitado ? __('Habilitado') : __('Deshabilitado'); ?></td>
                
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $usuario->idusuario]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $usuario->idusuario]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $usuario->idusuario], ['confirm' => __('Desea eliminar a {0}?', $usuario->email)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}} en total')]) ?></p>
    </div>
</div>
