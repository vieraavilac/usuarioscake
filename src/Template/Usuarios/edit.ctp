<title>Editar usuario <?= $this->Number->format($usuario->idusuario) ?></title>
<nav class="medium-2 medium-2 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $usuario->idusuario],
                ['confirm' => __('Desea eliminar a {0}?', $usuario->email)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Cerrar sesión'), ['action' => 'logout']) ?></li>
    </ul>
</nav>
<div class="usuarios form large-10 medium-8 columns content">
    <fieldset>
        <legend><?= __('Editar Usuario ') ?><?= $this->Number->format($usuario->idusuario) ?></legend>
        <?php
            $foto = '\\usuarios\\photo\\' . $usuario->photo_dir . '\\' . $usuario->photo;
            //echo  $foto;
            echo $this->Html->image($foto, ['alt' => 'Sin foto']);
            echo $this->Form->create($usuario, ['type' => 'file']);
            echo $this->Form->input('photo', ['type' => 'file']);
            // echo $this->Form->button(__('Subir'));
            

            echo $this->Form->control('email',['disabled']);
            echo $this->Form->control('apellidopaterno');
            echo $this->Form->control('apellidomaterno');
            echo $this->Form->control('nombre');
            echo $this->Form->control('habilitado');
            echo $this->Form->control('administrador');
            
           
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
