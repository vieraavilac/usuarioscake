		<?php
		        $base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz';
		        $base .= '1234567890';
		        $base .= '!@#%.,*?-_';

		        $contrasenia = '';
		        $limite = strlen($base) - 1;

		        for ($i=0; $i < 10; $i++)
		            $contrasenia .= $base[rand(0,$limite)];

		?>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../../webroot/css/main.css" />
		<noscript><link rel="stylesheet" href="../../webroot/css/noscript.css" /></noscript>