<?php
namespace App\Mailer; 
use Cake\Mailer\Mailer;    
class UsuarioMailer extends Mailer
{
public function registro($usuario)
{
// attach a text file 
$this->setTo($usuario->email) // add email recipient
->emailFormat('html') // email format
->setSubject(sprintf('Bienvenido %s', $usuario->name)) //  subject of email
->viewVars([   // these variables will be passed to email template defined in step 5 with . CAMBIAR A SETVIEWVARS
//name registered.ctp'username'=> $usuario->name,
'email'=>$usuario->email
])
// ->attachments([
// // attach an image file 
// 'edit.png'=>[
// 'file'=>'files/welcome.png',
// 'mimetype'=>'image/png',
// 'contentId'=>'734h3r38'
// ]
// ])
// the template file you will use in this email
->template('registro', 'custom') // By default template with same name as method name is used.
// the layout  'default.ctp’ file you will use in this email
->layout(default); //optional field
}}

//N4Jnhn8-gK