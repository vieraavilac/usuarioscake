<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usuarios Model
 *
 * @method \App\Model\Entity\Usuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario findOrCreate($search, callable $callback = null, $options = [])
 */
class UsuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        // $this->hasMany('Bitacoras')
        //      ->setForeignKey('idusuario');
        $this->setTable('usuarios');
        $this->setDisplayField('idusuario');
        $this->setPrimaryKey('idusuario');


        $this->addBehavior('Proffer.Proffer', [
            'photo' => [    // The name of your upload field
                'root' => WWW_ROOT . 'img', // Customise the root upload folder here, or omit to use the default
                'dir' => 'photo_dir'  // The name of the field to store the folder
             
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        $validator
            ->integer('idusuario')
            ->allowEmptyString('idusuario', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', false);

        $validator
            ->scalar('password')
            ->maxLength('password', 15)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);

        $validator
            ->scalar('apellidopaterno', 'No se permiten caracteres especiales ni numeeeéricos')
            ->maxLength('apellidopaterno', 50)
            ->requirePresence('apellidopaterno', 'create')
            ->allowEmptyString('apellidopaterno', false)
            ->add('apellidopaterno', 'validFormat',[
                'rule' =>  array('custom', '/^[A-ZÑÁÉÍÓÚ\sa-zñáéíóú]*$/'),
                'message' => 'No se permiten caracteres especiales ni numéricos'] );


        $validator
            ->scalar('apellidomaterno', 'No se permiten caracteres especiales ni numéricos') 
            ->maxLength('apellidomaterno', 50)
            ->allowEmptyString('apellidomaterno')
            ->add('apellidopaterno', 'validFormat',[
                'rule' =>  array('custom', '/^[A-ZÑÁÉÍÓÚ\sa-zñáéíóú]*$/'),
                'message' => 'No se permiten caracteres especiales ni numéricos'] );
;
        $validator
            ->scalar('nombre', 'No se permiten caracteres especiales ni numéricos')
            ->maxLength('nombre', 59)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false)
            ->add('apellidopaterno', 'validFormat',[
                'rule' =>  array('custom', '/^[A-ZÑÁÉÍÓÚ\sa-zñáéíóú]*$/'),
                'message' => 'No se permiten caracteres especiales ni numéricos'] );

        $validator
            ->boolean('habilitado')
            ->requirePresence('habilitado', 'create')
            ->allowEmptyString('habilitado', false);

        $validator
            ->boolean('administrador')
            ->requirePresence('administrador', 'create')
            ->allowEmptyString('administrador', false);

        // $validator
        //     ->requirePresence('photo', 'create')
        //     ->notEmpty('photo');


        $validator
            ->allowEmptyFile('photo', true);

        $validator
            ->sameAs('password1', 'password2', 'Verifica que la contraseña coincida.', 'update');

        $validator->provider('proffer', 'Proffer\Model\Validation\ProfferRules')
            // ->add('photo', 'proffer', [
            //       'rule' => ['filesize', '10MB'],
                  //'message' => 'Máximo 1MB'])
              //    'provider' => 'proffer'])
            ->allowEmptyFile('update')
            ->add('photo', 'proffer', [
                  'rule' => ['extension', ['jpg', 'jpeg', 'png']],
                  'message' => 'Extensión inválida',
                  'provider' => 'proffer'
            ]);

        // $validator->add('photo', [
        //     'tamanio' => [
        //         'rule' => 'fileSize', '<=', '1MB',
        //         'message' => 'Máxasassimo 1MB'
        //         ]
        //     ]);

        return $validator;
    }

    /*
    public function validationPhotos($validator){
        $validator->add('photo', [
            'validExtension' => [
                'rule' => ['extension',['png', 'jpg', 'jpeg']],
                'message' => 'Extensión inválida']
            ]);
        $validator->add('photo', [
            'tamanio' => [
                'rule' => ['filesize', '1MB'],
                'message' => 'Máximo 1MB'
            ]
            ]);

        return $validator;
    } */

    public function validationPasswords($validator){
        $validator->add('password2', 'no-misspelling', [
            'rule' => ['compareWith', 'password1'],
            'message' => 'Verifica que las contraseñas coincidan.',
        ]);

        return $validator;
    }

    public function validationRobustez($validator){
        //$regex = '/^([A-Z]+[a-z]+[\d]+[\w]+)*$/';V#dpfy-xf0

        $validator->add('password2','validFormat',[
                'rule' =>  array('custom', '/^([A-Z]+[a-z]+[\d]+[\W]+)*$/'),
                'message' => 'Debe contener al menos un número, un caracter especial, una mayúscula y una minúscula.'])
                  ->minLength('password2', 8, 'Muy corta')
                  ->maxLength('password2', 15, 'Muy larga');


        return $validator; 

    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }


}
