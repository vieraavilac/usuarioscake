<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bitacora Model
 *
 * @method \App\Model\Entity\Bitacora get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bitacora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bitacora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bitacora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bitacora|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bitacora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bitacora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bitacora findOrCreate($search, callable $callback = null, $options = [])
 */
class BitacorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        // $this->belongsTo('Usuarios')
        //     ->setForeignKey('idusuario');
        //     //->setJoinType('INNER');
        $this->setTable('bitacoras');
        $this->setPrimaryKey('idbitacora');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idbitacora')
            ->allowEmptyString('idbitacora', 'create');

        $validator
            ->integer('idusuario')
            ->requirePresence('idusuario', 'create')
            ->allowEmptyString('idusuario', false);

        $validator
            ->scalar('accion')
            ->requirePresence('accion', 'create')
            ->allowEmptyString('accion', false);

        $validator
            ->dateTime('fechahora')
            ->requirePresence('fechahora', 'create')
            ->allowEmptyDateTime('fechahora', false);

        return $validator;
    }


}
