CREATE DATABASE usuariosCake;

\c usuarioscake;


CREATE SEQUENCE seq_usuarios
INCREMENT BY 1

MINVALUE 1

START WITH 1

NO CYCLE;



CREATE TABLE usuarios(
	idUsuario INTEGER NOT NULL DEFAULT nextval('seq_usuarios'),
	email VARCHAR(50) NOT NULL,
	password VARCHAR(15) NOT NULL,
	apellidoPaterno VARCHAR(50) NOT NULL,
	apellidoMaterno VARCHAR(50),
	nombre VARCHAR(59) NOT NULL,
	habilitado BOOLEAN NOT NULL,
	administrador BOOLEAN NOT NULL,
	timeout timestamp DEFAULT NULL,
	photo VARCHAR(255),
	photo_dir VARCHAR(255),
	CONSTRAINT pk_usuario PRIMARY KEY (idUsuario)

);

CREATE SEQUENCE seq_bitacora
INCREMENT BY 1

MINVALUE 1

START WITH 1

NO CYCLE;


CREATE TYPE acciones AS ENUM ('Login', 'Logout', 'Solicitud de recuperacion de contrasenia', 'Cambio de contrasenia', 'Registro de usuario', 'Modificacion de usuario', 'Eliminacion de usuario');

CREATE TABLE bitacoras(
	idBitacora INTEGER NOT NULL DEFAULT nextval('seq_bitacora'),
	idUsuario INTEGER NOT NULL,
	accion ACCIONES NOT NULL,
	fechaHora timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);	
